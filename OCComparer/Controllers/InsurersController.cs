﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using OCComparer.Models;
using OCComparer.Repositories;

namespace OCComparer.Controllers
{
    public class InsurersController : Controller
    {
        private readonly InsurerRepository _context;

        public InsurersController()
        {
            _context = new InsurerRepository(new MongoClient("mongodb://localhost:27017"));
        }

        // GET: Insurers
        public async Task<IActionResult> Index()
        {
            return View(await _context.GetAllInsurers());
        }

        // GET: Insurers/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var insurer = await _context.GetInsurer(id);
            if (insurer == null)
            {
                return NotFound();
            }

            return View(insurer);
        }

        // GET: Insurers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Insurers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,LogoURL,SiteURL,Formula")] Insurer insurer)
        {
            if (ModelState.IsValid)
            {
                insurer.Id = Guid.NewGuid();
                await _context.InsertInsurerAsync(insurer);
                return RedirectToAction(nameof(Index));
            }
            return View(insurer);
        }

        // GET: Insurers/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var insurer = await _context.GetInsurer(id);
            if (insurer == null)
            {
                return NotFound();
            }
            return View(insurer);
        }

        // POST: Insurers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,Name,LogoURL,SiteURL,Formula")] Insurer insurer)
        {
            if (id != insurer.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _context.UpdateInsurer(id, insurer);
                }
                catch (Exception)
                {
                    if (!InsurerExists(insurer.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(insurer);
        }

        // GET: Insurers/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var insurer = await _context.GetInsurer(id);
            if (insurer == null)
            {
                return NotFound();
            }

            return View(insurer);
        }

        // POST: Insurers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            await _context.DeleteInsurer(id);
            return RedirectToAction(nameof(Index));
        }

        private bool InsurerExists(Guid id)
        {
            var c = _context.GetInsurer(id);
            return c.IsCompletedSuccessfully;
        }
    }
}
