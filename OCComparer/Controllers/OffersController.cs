﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using OCComparer.Models;
using OCComparer.Repositories;

namespace OCComparer.Controllers
{
    
    public class OffersController : Controller
    {
        private readonly InsurerRepository _insurers;
        private readonly CarRepository _cars;
        Random r;

        public OffersController()
        {
            _insurers = new InsurerRepository(new MongoClient("mongodb://localhost:27017"));
            _cars = new CarRepository(new MongoClient("mongodb://localhost:27017"));
            r = new Random();
        }
        public async Task<IActionResult> Form()
        {
            Offer o = new Offer();
            IEnumerable <Car> c = await _cars.GetAllCars();
            IEnumerable<Insurer> i = await _insurers.GetAllInsurers();
            o.Car = c.ElementAt(r.Next(0, c.Count()));
            o.Insurer = i.ElementAt(r.Next(0, i.Count()));
            o.Price = double.Parse(o.Insurer.CalculateOC(o.Car));
            return View(o);
        }
        [HttpPost]
        public IActionResult GetOffers([Bind("Id,Brand,Model,Price,Year,Mileage")] Car car)
        {
            var insurers = _insurers.GetAllInsurers().Result;
            List<Offer> offers = new List<Offer>();
            foreach (var item in insurers)
            {
                offers.Add(new Offer {
                    Car = car,
                    Insurer = item,
                    Price = double.Parse(item.CalculateOC(car)) 
                });
            }
            offers = offers.OrderBy(o => o.Price).ToList();
            return View(offers);
        }
    }
}
