﻿using MongoDB.Driver;
using OCComparer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCComparer.Repositories
{
    public class InsurerRepository : MongoRepository<Insurer>
    {
        public InsurerRepository(IMongoClient client) : base(client)
        {
        }

        public async Task<IEnumerable<Insurer>> GetAllInsurers()
        {
            return await GetCollection().Find(insurer => true).ToListAsync();
        }
        public async Task<Insurer> GetInsurer(Guid? id)
        {
            return await GetCollection().Find(insurer => insurer.Id == id).FirstOrDefaultAsync();
        }
        public async Task<Insurer> UpdateInsurer(Guid? id, Insurer i)
        {
            return await GetCollection().FindOneAndReplaceAsync(insurer => insurer.Id == id, i);
        }
        public async Task<Insurer> DeleteInsurer(Guid? id)
        {
            return await GetCollection().FindOneAndDeleteAsync(insurer => insurer.Id == id);
        }

        public async Task InsertInsurerAsync(Insurer i)
        {
            await InsertOne(i);
        }
    }
}
