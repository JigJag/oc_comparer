﻿using MongoDB.Driver;
using OCComparer.Attributes;
using OCComparer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCComparer.Repositories
{
    public class MongoRepository<T> : IMongoRepository<T> where T : class
    {
        public IMongoDatabase Database { get; }
        public MongoRepository(IMongoClient client)
        {
            Database = client.GetDatabase("OCComparer");
        }
        public async Task InsertOne(T model)
        {
            var collectionName = GetCollectionName();
            var collection = Database.GetCollection<T>(collectionName);
            await collection.InsertOneAsync(model);
        }
        public IMongoCollection<T> GetCollection()
        {
            var collectionName = GetCollectionName();
            var collection = Database.GetCollection<T>(collectionName);
            return collection;
        }
        private static string GetCollectionName() {
            return (typeof(T).GetCustomAttributes(typeof(BsonCollectionAttribute), true).FirstOrDefault()
                as BsonCollectionAttribute).CollectionName;
        }
    }
}
