﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCComparer.Repositories.Interfaces
{
    public interface IMongoRepository<T> where T : class
    {
        Task InsertOne(T model);
    }
}
