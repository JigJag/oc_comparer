﻿using MongoDB.Driver;
using OCComparer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCComparer.Repositories
{
    public class CarRepository : MongoRepository<Car>
    {
        public CarRepository(IMongoClient client) : base(client)
        {
        }
        
        public async Task<IEnumerable<Car>> GetAllCars()
        {
            return await GetCollection().Find(car => true).ToListAsync();
        }
        public async Task<Car> GetCar(Guid? id)
        {
            return await GetCollection().Find(car => car.Id == id).FirstOrDefaultAsync();
        }
        public async Task<Car> UpdateCar(Guid? id, Car c)
        {
            return await GetCollection().FindOneAndReplaceAsync(car => car.Id == id, c);
        }
        public async Task<Car> DeleteCar(Guid? id)
        {
            return await GetCollection().FindOneAndDeleteAsync(car => car.Id == id);
        }

        public async Task InsertCarAsync(Car c)
        {
            await InsertOne(c);
        }
    }
}
