﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCComparer.Models
{
    public class Offer
    {
        private double price;

        public double Price
        {
            get { return price; }
            set { price = Math.Round(value, 2); }
        }

        public Car Car { get; set; }
        public Insurer Insurer { get; set; }
    }
}
