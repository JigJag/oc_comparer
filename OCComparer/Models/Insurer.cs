﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NCalc;
using OCComparer.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCComparer.Models
{
    [BsonCollection("Insurers")]
    public class Insurer
    {
        [BsonId]
        [BsonElement("id")]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("logo")]
        public string LogoURL { get; set; }

        [BsonElement("site")]
        public string SiteURL { get; set; }

        [BsonElement("formula")]
        public string Formula { get; set; }

        public string CalculateOC(Car c)
        {
            Expression e = new Expression(Formula);
            e.Parameters["Price"] = c.Price;
            e.Parameters["Year"] = c.Year;
            e.Parameters["Mileage"] = c.Mileage;
            return e.Evaluate().ToString();
        }
    }
}
