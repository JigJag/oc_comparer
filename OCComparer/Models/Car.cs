﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using OCComparer.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCComparer.Models
{
    [BsonCollection("Cars")]
    public class Car
    {
        [BsonId]
        [BsonElement("id")]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; set; }

        [BsonElement("brand")]
        public string Brand { get; set; }

        [BsonElement("model")]
        public string Model { get; set; }

        [BsonElement("price")]
        public double Price { get; set; }

        [BsonElement("year")]
        public int Year { get; set; }

        [BsonElement("mileage")]
        public double Mileage { get; set; }

    }
}
