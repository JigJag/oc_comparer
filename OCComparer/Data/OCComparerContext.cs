﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace OCComparer.Models
{
    public class OCComparerContext : DbContext
    {
        public OCComparerContext (DbContextOptions<OCComparerContext> options)
            : base(options)
        {
        }

        public DbSet<OCComparer.Models.Car> Car { get; set; }
    }
}
