﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Microsoft.EntityFrameworkCore;
using OCComparer.Models;
using OCComparer.Repositories;

namespace OCComparer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSingleton(new MongoClient("mongodb://localhost:27017"));


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Offers}/{action=Form}/{id?}");
            });

            CarRepository db = new CarRepository(new MongoClient("mongodb://localhost:27017"));
            long l = db.GetCollection().Find(car => true).CountDocuments();
            if(l == 0)
            {
                db.InsertOne(new Car { Brand = "Volvo", Model = "XC90", Mileage = 140000, Price = 70000, Year = 2015 });
                db.InsertOne(new Car { Brand = "Volvo", Model = "V40", Mileage = 75432, Price = 38000, Year = 2008 });
                db.InsertOne(new Car { Brand = "Citroen", Model = "Xara Picasso", Mileage = 50000, Price = 12800, Year = 2003 });
                db.InsertOne(new Car { Brand = "Volkswagen", Model = "Polo", Mileage = 280000, Price = 3000, Year = 1991 });
            }

            InsurerRepository db2 = new InsurerRepository(new MongoClient("mongodb://localhost:27017"));
            l = db2.GetCollection().Find(insurer => true).CountDocuments();
            if (l == 0)
            {
                db2.InsertOne(new Insurer {
                    Name = "Link4",
                    LogoURL = "https://ubezpieczamy-auto.pl/wp-content/uploads/2016/11/logo-link4-1.png",
                    SiteURL = "https://www.link4.pl/ubezpieczenie-samochodu",
                    Formula = "[Price] / 100 + ([Year] - 1900)*1.2 + ([Mileage] / 1000)*1.1"
                });


                db2.InsertOne(new Insurer
                {
                    Name = "Axa",
                    LogoURL = "https://axa.pl/files/assets/images/logo-axa.svg",
                    SiteURL = "https://axa.pl/samochod-ubezpieczenia/",
                    Formula = "[Price] / 100 + ([Year] - 1900)*1.1 + ([Mileage] / 1000)*1.3"
                });

                db2.InsertOne(new Insurer
                {
                    Name = "Aviva",
                    LogoURL = "https://upload.wikimedia.org/wikipedia/commons/1/1e/Aviva_Logo.svg",
                    SiteURL = "https://www.aviva.pl/ubezpieczenia/ubezpieczenie-samochodu/",
                    Formula = "[Price] / 98 + ([Year] - 1900)*1.3 + ([Mileage] / 1000)*1.05"
                });
            }
        }
    }
}
